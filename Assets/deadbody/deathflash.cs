﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathflash : MonoBehaviour
{
    public Material dmg_mat;
    Material origin_mat;
    public MeshRenderer mr_head;
    public SkinnedMeshRenderer mr_cloak;
    public GameObject dmgFX;
    // Start is called before the first frame update
    void Start()
    {
         origin_mat = mr_head.material;
        StartCoroutine(flash());

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator flash()
    {
        GameObject effect = Instantiate(dmgFX, transform);
        effect.GetComponent<ParticleSystemRenderer>().material = dmg_mat;
        Destroy(effect, 0.5f);
        mr_head.material = dmg_mat;
        mr_cloak.material = dmg_mat;
       
        mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        yield return new WaitForSeconds(0.05f);
        mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        yield return new WaitForSeconds(0.05f);

        mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        yield return new WaitForSeconds(0.05f);
        mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        yield return new WaitForSeconds(0.05f);
        
        mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        yield return new WaitForSeconds(0.05f);
        mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        yield return new WaitForSeconds(0.05f);
        

        mr_head.material = origin_mat;
        mr_cloak.material = origin_mat;
    }
}
