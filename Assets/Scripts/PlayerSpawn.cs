﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PlayerSpawn : MonoBehaviour
{
    public GameObject UIcanvas, UIPrefab;
    public GameObject shadowTutorial;
    string startSceneName = "tutorial";
    public List<GameObject> playerz = new List<GameObject>();
    public List<GameObject> UI = new List<GameObject>();
    public static List<LinkedPlayer> spawnedPlayers = new List<LinkedPlayer>();
    public List<PlayerState> playerState = new List<PlayerState>();
    public bool pressForNextScene = false;
    List<int> unpairedCtrl = new List<int>{0,1,2,3};
    List<int> pairedCtrl = new List<int>{};

    int winnerCtrl;
    
    bool isTransition = false, isFoundWinner = false;
    Scene scene;

    AudioManager audio;
    public string bgmName;
    void Awake()
    { 
        if(bgmName != ""){
            audio = FindObjectOfType<AudioManager>();
            StartCoroutine(audio.FadeIn(bgmName,2));
        }

        scene = SceneManager.GetActiveScene();
        if (scene.name == startSceneName)
        {
            spawnedPlayers = new List<LinkedPlayer>();
        }
        else if(scene.name == "Result"){
            ShowResult();
        }
        //if (scene.name != startSceneName)
        else
        {
            for (int i = 0; i < spawnedPlayers.Count; i++)
            {
                int j = spawnedPlayers[i].controllerNum;
                spawnedPlayers[i].player = SpawnPlayer(j);
                GameObject UI = PairUI(spawnedPlayers[i]);
                MatchUIColor(UI);
                PlaceUI();
            }
            
            StartCoroutine(CountDownToStart());
        }
        pressForNextScene = false;
    }

    void Update()
    {
        if (scene.name == startSceneName){
            //if player press button0(A) && controller not paired yet
            for (int i = 0; i < unpairedCtrl.Count; i++){
                if(Input.GetButtonDown("JoinP" + unpairedCtrl[i])){
                    int controller = unpairedCtrl[i];
                    PairCtrl(controller);
                    LinkedPlayer newBorn = new LinkedPlayer(controller,SpawnPlayer(controller));
                    spawnedPlayers.Add(newBorn);
                    print(spawnedPlayers.Count);
                    GameObject UI = PairUI(newBorn);
                    MatchUIColor(UI);
                    PlaceUI();
                    StartCoroutine(tutorialInvisible(newBorn.player));
                    return;
                }
            }

            //if button1(X) && controller paired
            for (int i = 0; i < pairedCtrl.Count; i++){
                if (Input.GetButtonDown("JoinP" + pairedCtrl[i])){
                    int controller = pairedCtrl[i];
                    StartCoroutine(WaitToDestroy(controller));
                    return;
                }
            }

            if(spawnedPlayers.Count > 0 && shadowTutorial!=null){
                shadowTutorial.SetActive(true);
            }
        }
        if(Input.GetKeyDown(KeyCode.O)){
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
        if(Input.GetKeyDown(KeyCode.P)){
                StartCoroutine(NextScene(0));
        }

        if(pressForNextScene){
            StartCoroutine(NextScene(5));

            // if(!isTransition && (Input.GetButtonDown("JoinP" + winnerCtrl) || 
            //                     Input.GetButtonDown("Fire1P" +  winnerCtrl) ||
            //                     Input.GetButtonDown("Fire2P" +  winnerCtrl) ||
            //                     Input.GetButtonDown("Fire3P" +  winnerCtrl)))
            // {
            //     isTransition = true;
            //     StartCoroutine(NextScene(0));
            // }
        }
    }

    void LateUpdate(){
        if(scene.name != startSceneName){
            foreach(var i in spawnedPlayers)
            {
                TrackPlayerState(i.player);
            }
            
            if(playerState.Count == 1){
                playerState[0].win = true;
            }
            if(scene.name != "Result"){
                Winner();
            }
        }        
        
    }

    GameObject SpawnPlayer(int i){
        GameObject spawnee = Instantiate(playerz[i]);
        return spawnee;
    }

    void PairCtrl(int i){
        pairedCtrl.Add(i);
        if(unpairedCtrl.Count != 0)
            unpairedCtrl.Remove(i);
    }

    void UnpairCtrl(int i){
        unpairedCtrl.Add(i);
        if(pairedCtrl.Count != 0)
            pairedCtrl.Remove(i);
    }
    IEnumerator tutorialInvisible(GameObject p){
        yield return new WaitForSeconds(1);
        Animator playerAnimator = p.GetComponentInChildren<Animator>();
        playerAnimator.speed = 0.5f;
        
        playerAnimator.SetTrigger("invisible");
        while (!playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("DissolveOut")) {
            yield return null;
        }
        //Now wait for them to finish
        while (playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("DissolveOut")) {
            
            yield return null;
        }
        playerAnimator.speed = 1f;
        yield break;
    }

    IEnumerator WaitToDestroy(int j){
        for(int i = 0; i < spawnedPlayers.Count; i++){
            if(j == spawnedPlayers[i].controllerNum && spawnedPlayers[i].isUnpairing == false){
                spawnedPlayers[i].isUnpairing = true;
                Animator playerAnimator = spawnedPlayers[i].player.GetComponentInChildren<Animator>();
                playerAnimator.speed = 2f;
                playerAnimator.SetTrigger("invisible");
                while (!playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("DissolveOut")) {
                    yield return null;
                }
                //Now wait for them to finish
                while (playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("DissolveOut")) {
                    yield return null;
                }
                spawnedPlayers[i].isUnpairing = false;
                DestroyUI(j);
                PlaceUI();
                Destroy(spawnedPlayers[i].player);
                spawnedPlayers.Remove(spawnedPlayers[i]);
                UnpairCtrl(j);
            }
        }
        yield break;
    }

    public GameObject CountDown;
    public Text CountDownNum;
    IEnumerator CountDownToStart() {
        yield return new WaitForSeconds(1);
        CountDown.SetActive(true);
        yield return new WaitForSeconds(1);
        CountDownNum.text = "3";
        yield return new WaitForSeconds(1);
        CountDownNum.text = "2";
        yield return new WaitForSeconds(0.5f);
        
        for (int i = 0; i < spawnedPlayers.Count; i++)
        {
            spawnedPlayers[i].player.GetComponentInChildren<Animator>().SetTrigger("invisible");
            spawnedPlayers[i].player.GetComponentInChildren<PlayerState>().damagable = true;
        }
        yield return new WaitForSeconds(0.5f);
        CountDownNum.text = "1";
        yield return new WaitForSeconds(1);
        CountDown.SetActive(false);

        yield break;
    }

    void DestroyUI(int j){
        for(int i = 0; i < UI.Count; i++){
            if(UI[i].GetComponent<player1ui>().myplayer.controllerNum == j){
                Destroy(UI[i]);
                UI.RemoveAt(i);
            }
        }
    }

    void TrackPlayerState(GameObject player){
        playerState.RemoveAll(item => item == null);
        if(player!=null && !playerState.Contains(player.GetComponent<PlayerState>()))
        {
            playerState.Add(player.GetComponent<PlayerState>());
        }
        for (int i = 0; i < playerState.Count; i++) {
            if(playerState.Count>1 && playerState[i].isDead == true){
                playerState.Remove(playerState[i]);
            }
        }
    }

    public AudioSource winSound;
    void Winner(){    
        if(playerState.Count == 1)
        {
            //winSound.Play();
            PlayerManager pm = GetComponent<PlayerManager>();
            pm.ShowWinner(playerState[0].gameObject.transform);
            playerState[0].win = true;
            Animator playerAnimator = playerState[0].gameObject.GetComponentInChildren<Animator>();
            
            for(int i = 0; i < spawnedPlayers.Count; i++){
                if(spawnedPlayers[i].player != null){
                    PlayerState tempState = spawnedPlayers[i].player.GetComponent<PlayerState>();
                    if( tempState == playerState[0] && !isFoundWinner){
                        isFoundWinner = true;
                        playerAnimator.SetTrigger("win");
                        print("set");
                        spawnedPlayers[i].winCount+=1;
                        print(spawnedPlayers[i].player.name + " " + spawnedPlayers[i].winCount);
                    }
                }
            }
            winnerCtrl = playerState[0].gameObject.GetComponent<RigidbodyCharacter>().controllernumber;
        }
        if(playerState[0].isDead == true){
            playerState.Remove(playerState[0]);
        }
    }

    GameObject PairUI(LinkedPlayer linkedPlayer){
        GameObject spawnUI = Instantiate(UIPrefab);
        spawnUI.GetComponent<player1ui>().myplayer = linkedPlayer;
        spawnUI.transform.parent = UIcanvas.transform;
        spawnUI.transform.SetAsFirstSibling();
        spawnUI.transform.localScale = new Vector3(0.3f, 0.3f, 1);
        UI.Add(spawnUI);
        return spawnUI;
    }

    void PlaceUI(){
        switch(UI.Count){
            case 0:
                break;
            case 1:
                UI[0].transform.position = new Vector3((Screen.width + 20)/2, 50, 0);
                break;
            case 2:
                UI[0].transform.position = new Vector3((Screen.width + 20)/3, 50, 0);
                UI[1].transform.position = new Vector3(2*(Screen.width + 20)/3, 50, 0);
                break;
            case 3:
                UI[0].transform.position = new Vector3((Screen.width + 20)/4, 50, 0);
                UI[1].transform.position = new Vector3(2*(Screen.width + 20)/4, 50, 0);
                UI[2].transform.position = new Vector3(3*(Screen.width + 20)/4, 50, 0);
                break;
            case 4:
                UI[0].transform.position = new Vector3((Screen.width + 20)/5, 50, 0);
                UI[1].transform.position = new Vector3(2*(Screen.width + 20)/5, 50, 0);
                UI[2].transform.position = new Vector3(3*(Screen.width + 20)/5, 50, 0);
                UI[3].transform.position = new Vector3(4*(Screen.width + 20)/5, 50, 0);
                break;
        }
    }

    void MatchUIColor(GameObject gameObject) {
        player1ui playerUI = gameObject.GetComponent<player1ui>();
        switch (playerUI.myplayer.controllerNum){
            case 0:
                playerUI.setColor(new Color(90f/255f,200f/255f,90f/255f,1));
                break;
            case 1:
                playerUI.setColor(new Color(1,85f/255f,85f/255f,1));
                break;
            case 2:
                playerUI.setColor(new Color(60f/255f,70f/255f,1,1));
                break;
            case 3:
                playerUI.setColor(new Color(200f/255f,0,1,1));
                break;
        }
    }

    public GameObject resultUI;
    public List<GameObject> ResultLayout = new List<GameObject>();
    public Transform resultBG;
    void ShowResult(){
        
        foreach(LinkedPlayer player in spawnedPlayers){
            GameObject spawnUI = Instantiate(resultUI);
            ResultUI result = spawnUI.GetComponent<ResultUI>();
            result.pair = player;
            //spawnUI.transform.parent = UIcanvas.transform;
            spawnUI.transform.SetParent(UIcanvas.transform,false);
            spawnUI.transform.SetAsFirstSibling();
            //spawnUI.transform.localScale = new Vector3(0.3f, 0.3f, 1);
            
            result.SetScore(result.pair.winCount);

            switch (result.pair.controllerNum){
                case 0:
                    result.SetColor(new Color(90f/255f,200f/255f,90f/255f,1));
                    break;
                case 1:
                    result.SetColor(new Color(1,85f/255f,85f/255f,1));
                    break;
                case 2:
                    result.SetColor(new Color(60f/255f,70f/255f,1,1));
                    break;
                case 3:
                    result.SetColor(new Color(200f/255f,0,1,1));
                    break;
            }
            
            ResultLayout.Add(spawnUI);
        }
        resultBG.SetAsFirstSibling();
        ResultLayout.Sort((p1,p2)=>p1.GetComponent<ResultUI>().pair.winCount.CompareTo(p2.GetComponent<ResultUI>().pair.winCount));
        ResultLayout.Reverse();

        float xPos = Screen.width/2 -120;
        Vector3 champScale = ResultLayout[0].transform.localScale;
        switch(ResultLayout.Count){
            case 0:
                break;
            case 1:
                ResultLayout[0].transform.position = new Vector3(Screen.width/2-100,Screen.height/2, 0);
                break;
            case 2:
                ResultLayout[0].transform.position = new Vector3(xPos - xPos* 0.15f +40, 2*(Screen.height - 50)/3, 0);
                ResultLayout[0].transform.localScale = new Vector3(champScale.x * 1.6f, champScale.y *1.6f, champScale.z);
                ResultLayout[1].transform.position = new Vector3(xPos, (Screen.height-50)/3, 0);
                break;
            case 3:
                ResultLayout[0].transform.position = new Vector3(xPos - xPos* 0.1f+20, 3*(Screen.height - 150)/4, 0);
                ResultLayout[0].transform.localScale = new Vector3(champScale.x * 1.4f, champScale.y * 1.4f, champScale.z);
                ResultLayout[1].transform.position = new Vector3(xPos, 2*(Screen.height - 150)/4, 0);
                ResultLayout[2].transform.position = new Vector3(xPos, (Screen.height-150)/4, 0);
                break;
            case 4: 
                ResultLayout[0].transform.position = new Vector3(xPos * 0.95f +10, 4*(Screen.height - 100)/5, 0);
                ResultLayout[0].transform.localScale = new Vector3(champScale.x * 1.2f, champScale.y * 1.2f, champScale.z);
                ResultLayout[1].transform.position = new Vector3(xPos, 3*(Screen.height - 100)/5, 0);
                ResultLayout[2].transform.position = new Vector3(xPos, 2*(Screen.height - 100)/5, 0);
                ResultLayout[3].transform.position = new Vector3(xPos, (Screen.height-100)/5, 0);
                break;
        }
    }

    public Animator transition;
    IEnumerator NextScene(int time){
        
        yield return new WaitForSeconds(time);
        if(bgmName != ""){
            StartCoroutine(audio.FadeOut(bgmName,1));
        }
        transition.SetTrigger("fadeOut");
        yield return new WaitForSeconds(2);
        if(SceneManager.GetActiveScene().buildIndex >= 5){
            SceneManager.LoadScene(0);
        }
        else{
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}

public class LinkedPlayer{
    public int controllerNum, winCount;
    public GameObject player;
    public bool isUnpairing;
    public LinkedPlayer(int i, GameObject p){
        controllerNum = i;
        player = p;
        isUnpairing = false;
        winCount = 0;
    }

}
