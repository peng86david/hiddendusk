﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
public class PlayerManager : MonoBehaviour
{

    List<GameObject> players = new List<GameObject>();
    List<PlayerState> playerState = new List<PlayerState>();
    public GameObject winUI, spotLight = null;
    public Light mainLight;

    bool spotlightLit = false, isWin = false;
    void Start()
    {
        if(GameObject.FindGameObjectsWithTag("player0").Length > 0)
            players.Add(GameObject.FindGameObjectsWithTag("player0")[0]);

        if(GameObject.FindGameObjectsWithTag("player1").Length > 0){
            players.Add(GameObject.FindGameObjectsWithTag("player1")[0]);}

        if(GameObject.FindGameObjectsWithTag("player2").Length > 0){
            players.Add(GameObject.FindGameObjectsWithTag("player2")[0]);}
        
        if(GameObject.FindGameObjectsWithTag("player3").Length > 0)
            players.Add(GameObject.FindGameObjectsWithTag("player3")[0]);

        
        foreach (GameObject player in players) {
            playerState.Add(player.GetComponent<PlayerState>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        playerState.RemoveAll(item => item == null);
        if (spotlightLit) {
            isWin = true;
            print("isLit");
            spotlightLit = false;
            StartCoroutine(WinnerSpotlight());
        }
    }

    void ShowWinScreen() {
        
        winUI.SetActive(true);
    }

    public GameObject winTxt;

    IEnumerator WinnerSpotlight() {
        spotLight.SetActive(true);
        float tempRange = spotLight.GetComponent<Light>().range;
        spotLight.GetComponent<Light>().range = 5f;
        for (int i = 0; i < 3 * 60; i++)
        {
            mainLight.spotAngle -= 100f / 180f;
            spotLight.GetComponent<Light>().spotAngle += 90f / 180f;
            yield return new WaitForFixedUpdate();
        }
        //spotLight.GetComponent<Light>().spotAngle = 180f;
        //mainLight.spotAngle = 0;
        spotLight.GetComponent<Light>().range = tempRange;
        spotLight.GetComponent<AudioSource>().Play();
        DOTween.To(()=> Camera.main.fieldOfView, x=> Camera.main.fieldOfView = x, 50, 0.5f);

        ShowWinScreen();
        yield return new WaitForSeconds(1f);
        winTxt.SetActive(true);
        GetComponent<PlayerSpawn>().pressForNextScene = true;
        mainLight.gameObject.SetActive(false);

        yield return new WaitForFixedUpdate();
    }

    public void ShowWinner( Transform trans){
        for (int i = 0; i < playerState.Count; i++) {
            if(playerState.Count <= 1 && !isWin ){
                spotLight.transform.position = new Vector3(trans.position.x, trans.position.y+10, trans.position.z);
                spotLight.transform.parent = trans;
                //spotLight.SetActive(true);
                spotlightLit = true;
            }
            
        }
    }
}
