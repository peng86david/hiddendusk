﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StartSceneManager : MonoBehaviour
{
    List<GameObject> players = new List<GameObject>();
    public GameObject play;
    PlayerSpawn playerSpawn;
    public Animator transition;
    bool isTransition;

    AudioManager audio;
    // Start is called before the first frame update
    void Awake()
    {
        audio = FindObjectOfType<AudioManager>();
        StartCoroutine(audio.FadeIn("TutorialBGM",2));

        playerSpawn = GetComponent<PlayerSpawn>();
        isTransition = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (play == null && !isTransition) {
            if (playerSpawn.UI.Count > 1) {
                isTransition = true;
                StartCoroutine(NextScene());
            }
            else
                print("needs more players");
        }
    }

    IEnumerator NextScene(){
        
        StartCoroutine(audio.FadeOut("TutorialBGM",1));

        transition.SetTrigger("fadeOut");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Desert");
    }
}
