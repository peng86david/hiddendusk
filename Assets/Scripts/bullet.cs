﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class bullet : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject detect;
    private Rigidbody rb;
    //public ParticleSystem trail;
    public GameObject burst;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != this.tag && collision.gameObject.layer != LayerMask.NameToLayer("particle"))
        {
            //burst.transform.parent = null;
            //trail.transform.parent = null;
            //burst.gameObject.SetActive(true);
            //trail.Stop();
            //Destroy(trail.gameObject, 1);
            //Destroy(burst, .5f);

            if (collision.gameObject.layer == LayerMask.NameToLayer("ground"))
            {
                detect.transform.position = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);
                detect.SetActive(true);
                detect.transform.parent = null;
                rb.isKinematic = true;
                Destroy(gameObject);
                
                
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("player") && collision.gameObject.tag != this.tag)
            {
                PlayerState stateChange = collision.gameObject.GetComponent<PlayerState>();
                stateChange.attackdirect = gameObject.transform.up;
                //bursteff
                burst.transform.parent = null;
                burst.transform.position = collision.transform.position;
                burst.gameObject.SetActive(true);
                Destroy(burst, .5f);
                if (stateChange.damagable)
                {
                    stateChange.currentHp -= 1;
                    stateChange.TakeDamage(0);
                }
                Destroy(gameObject);
            }
            else
            {
                detect.transform.position = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);
                detect.SetActive(true);
                detect.transform.parent = null;
                
               

                rb.isKinematic = true;
                
                Destroy(gameObject);

            }
        }

        
    }
}
