﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    public Material dmg_mat;
    Material origin_mat;
    public int hpMax = 5;
    public int currentHp;
    public bool isDead, isShown = false, win;
    public MeshRenderer mr_head;
    public SkinnedMeshRenderer mr_cloak;
    public GameObject dmgFX;
    public Material blurMat;
    public Vector3 attackdirect;
    public GameObject deadbody;
    public bool damagable;
    // Start is called before the first frame update
    void Start()
    {
        origin_mat = mr_head.material;
        currentHp = hpMax;
        isDead = false;
        win = false;
        if (gameObject.name != "Cube")
        {
            damagable = false;
        }
        else {
            damagable = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isShown){
            StartCoroutine(showInvis());
            isShown = false;
        }
    }

    IEnumerator showInvis() {
        if (mr_cloak != null)
        {
            mr_head.material = blurMat;
            mr_cloak.material = blurMat;

            mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
            mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        }
        yield return new WaitForSeconds(3);
        mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        mr_head.material = origin_mat;
        mr_cloak.material = origin_mat;
    }

    public void TakeDamage(float t) {
        if(!win)
            StartCoroutine(flash(t));
    }

    IEnumerator flash(float t) {
        yield return new WaitForSeconds(t);
        GameObject effect = Instantiate(dmgFX,transform);
        effect.GetComponent<ParticleSystemRenderer>().material = dmg_mat;
        Destroy(effect,0.5f);
        if (currentHp>0){
            if(mr_cloak != null){
                mr_head.material = dmg_mat;
                mr_cloak.material = dmg_mat;
                for(int i = 0; i<3; i++) {
                    mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                    mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                    yield return new WaitForSeconds(0.05f);
                    mr_head.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                    mr_cloak.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                    yield return new WaitForSeconds(0.05f);
                }
                mr_head.material = origin_mat;
                mr_cloak.material = origin_mat;
            }
        }
        
        //yield return new WaitForSeconds(0.05f);
        if (currentHp <= 0 && !win)
        {
            isDead = true;
            if(deadbody != null){
                GameObject flyingbody = Instantiate(deadbody,gameObject.transform.position,gameObject.transform.rotation,null);
                flyingbody.GetComponent<Rigidbody>().AddForce(attackdirect*-20,ForceMode.Impulse);
            }
                
            Destroy(gameObject);
        }
    }
}
