﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    public Animator transition;
    AudioManager audio;
    // Start is called before the first frame update
    void Start()
    {
        audio = FindObjectOfType<AudioManager>();
        StartCoroutine(audio.FadeIn("StartBGM",3));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape) && !Input.GetKeyUp(KeyCode.Escape)) {
            StartCoroutine(Next());
        }

        if(Input.GetKeyDown(KeyCode.Escape)){
            StartCoroutine(Exit());
            
        }

    }

    IEnumerator Next()
    {
        StartCoroutine(audio.FadeOut("StartBGM",1));
        transition.SetTrigger("transition");
        yield return new WaitForSeconds(1.3f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    IEnumerator Exit()
    {
        StartCoroutine(audio.FadeOut("StartBGM",1));
        transition.SetTrigger("transition");
        yield return new WaitForSeconds(1.3f);
        Application.Quit();
        print("exit");
    }
}
