﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
//full explanation on https://www.youtube.com/watch?v=6OT43pvUyfY

public class AudioManager : MonoBehaviour {

	public Sound[] sounds;
	public AudioMixerGroup BgmMixer;
	// Scene m_Scene;
	// Scene f_Scene;
	public static AudioManager instance;

	// Use this for initialization
	void Awake () {
		//m_Scene = SceneManager.GetActiveScene();
		//the music will continue when loading a new scene
		DontDestroyOnLoad (gameObject);
		if (instance == null)
			instance = this;
		else {
			Destroy (gameObject);
			return;
		}

		foreach (Sound s in sounds) {
			s.source = gameObject.AddComponent<AudioSource> ();
			s.source.clip = s.clip;
			s.source.outputAudioMixerGroup = BgmMixer;

			s.source.volume = s.volume;
			s.source.pitch = s.pitch;
			s.source.loop = s.loop;
		}

	}

	void Start(){
		
	}

	void Update(){
		// m_Scene = SceneManager.GetActiveScene();
		// if (m_Scene.buildIndex != f_Scene.buildIndex){
		// 	sounds[m_Scene.buildIndex].source.Play();
		// 	int j = f_Scene.buildIndex;
		// 	if(j>=0 && j <= 6)
		// 		sounds[f_Scene.buildIndex].source.Stop();
		// }
		// f_Scene = SceneManager.GetActiveScene();
	}

	public void play(string name){
		Sound s = Array.Find (sounds, sound => sound.name == name);
		if (s == null) {
			Debug.Log ("sound doesn't exist!!!");
			return;
		}
		s.source.Play ();
		Debug.Log("playing~");
	}

	public IEnumerator FadeIn(string name, float duration){
		Sound s = Array.Find (sounds, sound => sound.name == name);
		float targetVol = s.source.volume;
		s.source.volume = 0;
		s.source.Play ();

		for(float i = 0; i < duration; i+= 0.1f){
			yield return new WaitForSeconds(0.1f);
			s.source.volume += 0.1f/duration;
		}
	}

	public IEnumerator FadeOut(string name, float duration){
		Sound s = Array.Find (sounds, sound => sound.name == name);
		for(float i = 0; i < duration; i+= 0.1f){
			yield return new WaitForSeconds(0.1f);
			s.source.volume -= 0.1f/duration;
		}
		if(s.source.volume <= 0){
			s.source.Stop();
		}
	}
}
