﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class resultPress : MonoBehaviour
{
    public Animator transition;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape) && !Input.GetKeyUp(KeyCode.Escape)) {
            StartCoroutine(Next());
        }
    }

    IEnumerator Next()
    {
        transition.SetTrigger("fadeOut");
        yield return new WaitForSeconds(1.3f);
        SceneManager.LoadScene(0);
    }
}
