﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ResultUI : MonoBehaviour
{
    public LinkedPlayer pair;
    public Text score;
    public Image bg;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetColor(Color col){
        bg.color = col;
    }
    public void SetScore(float s){
        score.text = s.ToString();
    }
}
