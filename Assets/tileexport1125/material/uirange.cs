﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uirange : MonoBehaviour
{   Color randomdark;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        randomdark = gameObject.GetComponent<Image>().color;
        if (randomdark.a >= 230)
        {
            randomdark.a = Mathf.Lerp(randomdark.a, 255, 0.2f);
        }
        else if (randomdark.a<= 50)
        {
           randomdark.a = Mathf.Lerp(randomdark.a, 0, 0.2f);
        }
        gameObject.GetComponent<Image>().color = randomdark;
    }
}
