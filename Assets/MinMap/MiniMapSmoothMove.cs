﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapSmoothMove : MonoBehaviour
{
    public Transform target1;
    public Transform target2;
    public Transform target3;
    public Transform target4;
    public float Smoothspeed = 0.125f;
    public Vector3 offset;

    void FixedUpdate()
    {
        Vector3 desiredPostion = target1.position + offset;
        Vector3 smoothedPostion = Vector3.Lerp(transform.position, desiredPostion, Smoothspeed);
        transform.position = smoothedPostion;

    }
}
