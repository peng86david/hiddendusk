﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapVisableControl : MonoBehaviour
{
    public GameObject MiniMap;
    public Material MiniMapAlphaMaterial;
    private bool isShowing;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("="))
        {
            isShowing = !isShowing;
            //color.a = 0.4f;
            //Debug.Log("Colorchange");   
            MiniMap.SetActive(isShowing);
        }
        
    }
}
