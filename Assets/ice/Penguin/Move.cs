﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Move : MonoBehaviour
{
    public Vector3 originalPos;
    public Vector3 targetPos;
    public float duration;
    void Start()
    {
        MoveToTargetPos();
    }
    void MoveToTargetPos()
    {
        Quaternion q = Quaternion.LookRotation(targetPos-originalPos);
        transform.DORotateQuaternion(q,1f).OnComplete(()=>transform.DOMove(targetPos,duration).OnComplete(()=>MoveToOriginalPos()));
        
    }
    void MoveToOriginalPos()
    {
        Quaternion q = Quaternion.LookRotation(originalPos-targetPos);
        transform.DORotateQuaternion(q,1f).OnComplete(()=>transform.DOMove(originalPos,duration).OnComplete(()=>MoveToTargetPos()));
    }
}
